//
//  BinaryTree.swift
//  InorderIteratorForBinaryTree
//
//  Created by vishnusankar on 20/06/18.
//  Copyright © 2018 vishnusankar. All rights reserved.
//

import Foundation

class Node<T : Comparable> {
    var leftNode : Node<T>?
    var rightNode : Node<T>?
    var value : T
    
    init(value : T) {
        self.value = value
    }
}

class BinaryTree<T : Comparable> {
    var rootNode : Node<T>
    
    init(rootNode : Node<T>) {
        self.rootNode = rootNode
    }
    
    func addNode(newNode : Node<T>) {
        var currentNode : Node<T>? = self.rootNode
        while currentNode != nil {
            
            
            if newNode.value > (currentNode?.value)! {
                if currentNode?.rightNode != nil {
                    currentNode = currentNode?.rightNode!
                }else {
                    currentNode?.rightNode = newNode
                    currentNode = nil
                }
            }else {
                if currentNode?.leftNode != nil {
                    currentNode = currentNode?.leftNode!
                }else {
                    currentNode?.leftNode = newNode
                    currentNode = nil
                }
            }
        }
    }
    enum Order {
        case preOrder
        case inOrder
        case postOrder
    }
    func inOrderIteratorBinaryTree(currentNode : Node<T>?, order : Order) {
        if currentNode == nil {
            return
        }
        if order == .preOrder {
            print(currentNode?.value)
        }
        self.inOrderIteratorBinaryTree(currentNode: currentNode?.leftNode, order: order)
        if order == .inOrder {
            print(currentNode?.value)
        }
        self.inOrderIteratorBinaryTree(currentNode: currentNode?.rightNode, order: order)
        if order == .postOrder {
            print(currentNode?.value)
        }
    }
    
}
